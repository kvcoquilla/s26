const http = require('http');

const PORT = 3000;

http.createServer(function(request, response){

	if(request.url == '/hello'){

		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('Welcome to the login page')
	} else {
	
	// This will be our response if an endpoint passed in the client's request is not recognized or there is no designated route for this endpoint.
		response.writeHead(404,{'Content-Type': 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found'.)
	}

}).listen(PORT);

console.log(`Server is running at localhost:${PORT}.`)
